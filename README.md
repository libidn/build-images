# Build images for libidn/libidn2

This libidn sub-project generates and pushes to gitlab.com
container registry the build images to be used for building the
the library in various environments.

The reason for pre-generating the images is to speed-up CI runs
and avoid failures due to downloading of individual packages (e.g.,
because some mirrors were down).


# How to generate a new image

Add a new directory with a Containerfile containing the instructions
for the image.

Then edit .gitlab-ci.yml to add the build instructions, commit and push.


# How to re-generate existing images

Visit the [Pipeline page](https://gitlab.com/libidn/build-images/pipelines)
and click on 'Run Pipeline'.

The images will be re-build.


# Building locally

```
buildah login registry.gitlab.com
buildah build -t registry.gitlab.com/libidn/build-images:libidn2-Debian12 libidn2-Debian12
buildah push registry.gitlab.com/libidn/build-images:libidn2-Debian12
```

# Build and test outside Gitlab registry

```
$ podman build libidn2-Debian12
...
COMMIT
--> 95f80f7db3a
95f80f7db3ad3f10252a76e3de4b50de806bef0fb20b25c90ccaf5890c2b186d
$ podman run -it 95f80f7db3a /bin/bash
# git clone https://gitlab.com/libidn/libidn2.git
# cd libidn2
...
```
